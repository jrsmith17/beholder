use std::process::Command;

fn main(){
	println!("Hello from build script");

	if cfg!(target_os = "windows") {
    	Command::new("cmd")
            .args(&["/C", "pushd && cd lib/temp && cargo clean && rustc --crate-type=dylib && popd"])
            .spawn()
            .expect("failed to execute process")
	} else {
    	Command::new("sh")
            .arg("-c")
            .arg("pushd && cd lib/temp && cargo build --release && popd")
            .spawn()
            .expect("failed to execute process")
	};

	//let path = "lib/temp/target/release";
    //let lib = "libtemp";

    //println!("cargo:rustc-link-search=native={}", path);
    //println!("cargo:rustc-link-lib=dylib={}", lib);
}