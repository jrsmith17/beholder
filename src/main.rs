//use test;
extern crate libloading as lib;

/*fn call_dynamic() -> Result<u32, Box<dyn std::error::Error>> {
    let lib = lib::Library::new("lib/temp/target/release/libtemp.rlib")?;
    unsafe {
        let func: lib::Symbol<unsafe extern fn() -> u32> = lib.get(b"test")?;
        Ok(func())
    }
}*/

fn main() {
    println!("Hello, world!");
    //test::test();

    //let func: lib::Symbol<unsafe extern fn() -> u32> = call_dynamic().unwrap();
    let lib = lib::Library::new("lib/temp/target/release/temp.dll").unwrap();
    /*let lib : lib::Library = match lib::Library::new("lib/temp/target/release/libtemp.rlib"){
    	Ok(rlib) => {
    		rlib
    	},
    	Err(_) => {
    		println!("Error galore.");
    		lib::Library(lib::os::windows::Library::new())
    	}
    };*/
    unsafe{
    	let f : lib::Symbol<unsafe extern fn()> = lib.get(b"my_test").unwrap();
    	//f();
	}
    /*unsafe {
    	let func : lib::Symbol<unsafe extern fn() -> u32>;
        match lib.get(b"test") {
        	Ok(f) => {
        		func = f;
        		func();
        	},
        	Err(_) => {

        	}
        }
        
    }*/
}
